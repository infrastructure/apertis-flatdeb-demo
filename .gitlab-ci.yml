# vi:et:sts=2

variables:
  FLATDEB_PROJECT_URL: https://gitlab.apertis.org/infrastructure/apertis-flatdeb.git
  APERTIS_RELEASE_DEFAULT: "v2021"
  upload_host: archive@images.apertis.org
  upload_root_main: /srv/images/public/flatpak/repo
  upload_root_test: /srv/images/test/flatpak/flatdeb-demo/$CI_COMMIT_REF_SLUG
  remote_url_main: https://images.apertis.org/flatpak/repo
  remote_url_test: https://images.apertis.org/test/flatpak/flatdeb-demo/${CI_COMMIT_REF_SLUG}

image: registry.gitlab.apertis.org/infrastructure/apertis-docker-images/${APERTIS_RELEASE}-flatdeb-builder

stages:
  - prepare
  - build base
  - build runtime
  - build app
  - publish

prepare-build-env:
  image: debian:bullseye-slim
  stage: prepare
  script:
    - APERTIS_RELEASE=$(echo $CI_COMMIT_BRANCH | grep -o 'v[0-9]\{4\}\(dev[0-9]\|pre\)\?' || true)
    - APERTIS_RELEASE=${APERTIS_RELEASE:-$APERTIS_RELEASE_DEFAULT}
    - echo "APERTIS_RELEASE=$APERTIS_RELEASE" | tee -a build.env
    - $CI_PROJECT_DIR/generate-files.sh ${APERTIS_RELEASE}
    - |
      case "$CI_COMMIT_REF_NAME" in
        master|apertis/*)
          echo "UPLOAD_ROOT=$upload_root_main" | tee -a build.env
          echo "REMOTE_URL=$remote_url_main" | tee -a build.env
          ;;
        *)
          echo "UPLOAD_ROOT=$upload_root_test" | tee -a build.env
          echo "REMOTE_URL=$remote_url_test" | tee -a build.env
          ;;
      esac
  artifacts:
    reports:
      dotenv:
        - build.env
    paths:
      - suites/v*.yaml
      - apps/org.apertis.demo.gnome-font-viewer.yaml

.build:
  variables:
    GIT_STRATEGY: clone
  before_script:
    - chmod 0400 "$ARCHIVE_SECRET_FILE"
    - eval $(ssh-agent -s)
    - ssh-add "$ARCHIVE_SECRET_FILE"
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - if: $CI_COMMIT_BRANCH && $ARCHIVE_SECRET_FILE
      when: on_success
    - when: never

.build-amd64:
  variables:
    architecture: amd64
    flatpakarch: x86_64

.build-arm64:
  variables:
    architecture: arm64
    flatpakarch: aarch64
  tags:
    - kvm

.build-armhf:
  variables:
    architecture: armhf
    flatpakarch: arm
  tags:
    - kvm

.build-base:
  extends:
    - .build
  stage: build base
  script:
    - /opt/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo
        --suite=${APERTIS_RELEASE} --arch=${architecture} base
  artifacts:
    paths:
      - flatdeb-builddir/base-${APERTIS_RELEASE}-${architecture}.tar.gz

.build-runtime:
  extends:
    - .build
  stage: build runtime
  script:
    - /opt/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo
        --suite=${APERTIS_RELEASE} --arch=${architecture} --platform runtimes runtimes/org.apertis.demo.yaml
    - /opt/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo
        --suite=${APERTIS_RELEASE} --arch=${architecture} --sdk runtimes runtimes/org.apertis.demo.yaml
  artifacts:
    paths:
      - flatdeb-builddir/ostree-repo

.build-app:
  extends:
    - .build
  stage: build app
  script:
    - |
      if [ "${architecture}" != "amd64" ]; then
          /usr/sbin/update-binfmts --enable qemu-${flatpakarch}
      fi
    - /opt/apertis-flatdeb/run.py --build-area=$(pwd)/flatdeb-builddir --ostree-repo=$(pwd)/flatdeb-builddir/ostree-repo
        --suite=${APERTIS_RELEASE} --arch=${architecture} app --app-branch=${APERTIS_RELEASE} apps/org.apertis.demo.gnome-font-viewer.yaml
  artifacts:
    paths:
      - flatdeb-builddir/ostree-repo

.publish-runtime:
  extends:
    - .build
  stage: publish
  script:
    - RUNTIME_PREFIX=$(awk '/^id_prefix:/ {print $2; exit}' runtimes/org.apertis.demo.yaml)
    - RUNTIME_ID=${RUNTIME_PREFIX}.Platform
    - SDK_ID=${RUNTIME_PREFIX}.Sdk
    - $CI_PROJECT_DIR/ostree-prepare-local-repo.sh $(pwd)/flatdeb-builddir/ostree-repo ${REMOTE_URL} runtime/${RUNTIME_ID}/${flatpakarch}/${APERTIS_RELEASE}
    - $CI_PROJECT_DIR/ostree-prepare-local-repo.sh $(pwd)/flatdeb-builddir/ostree-repo ${REMOTE_URL} runtime/${SDK_ID}/${flatpakarch}/${APERTIS_RELEASE}
    - $CI_PROJECT_DIR/ostree-prepare-local-repo.sh $(pwd)/flatdeb-builddir/ostree-repo ${REMOTE_URL} runtime/${SDK_ID}.Debug/${flatpakarch}/${APERTIS_RELEASE}
    - $CI_PROJECT_DIR/ostree-prepare-local-repo.sh $(pwd)/flatdeb-builddir/ostree-repo ${REMOTE_URL} runtime/${SDK_ID}.Sources/${flatpakarch}/${APERTIS_RELEASE}
    - ssh -oStrictHostKeyChecking=no "$upload_host" mkdir -p "$UPLOAD_ROOT"
    - ssh -oStrictHostKeyChecking=no "$upload_host" ostree init --repo="${UPLOAD_ROOT}" --mode=archive-z2
    - ostree-push --repo $(pwd)/flatdeb-builddir/ostree-repo "$upload_host:${UPLOAD_ROOT}"
        runtime/${RUNTIME_ID}/${flatpakarch}/${APERTIS_RELEASE}
        runtime/${SDK_ID}/${flatpakarch}/${APERTIS_RELEASE}
        runtime/${SDK_ID}.Debug/${flatpakarch}/${APERTIS_RELEASE}
        runtime/${SDK_ID}.Sources/${flatpakarch}/${APERTIS_RELEASE}
    - ssh -oStrictHostKeyChecking=no "$upload_host" flatpak build-update-repo "$UPLOAD_ROOT"

.publish-app:
  extends:
    - .build
  stage: publish
  script:
    - APP_ID=$(awk '/^id:/ {print $2; exit}' apps/org.apertis.demo.gnome-font-viewer.yaml)
    - $CI_PROJECT_DIR/ostree-prepare-local-repo.sh --full-commit $(pwd)/flatdeb-builddir/ostree-repo ${REMOTE_URL} app/${APP_ID}/${flatpakarch}/${APERTIS_RELEASE}
    - $CI_PROJECT_DIR/ostree-prepare-local-repo.sh --full-commit $(pwd)/flatdeb-builddir/ostree-repo ${REMOTE_URL} runtime/${APP_ID}.Sources/${flatpakarch}/${APERTIS_RELEASE}
    - ssh -oStrictHostKeyChecking=no "$upload_host" mkdir -p "$UPLOAD_ROOT"
    - ssh -oStrictHostKeyChecking=no "$upload_host" ostree init --repo="${UPLOAD_ROOT}" --mode=archive-z2
    - ostree-push --repo $(pwd)/flatdeb-builddir/ostree-repo "$upload_host:${UPLOAD_ROOT}"
        app/${APP_ID}/${flatpakarch}/${APERTIS_RELEASE}
        runtime/${APP_ID}.Sources/${flatpakarch}/${APERTIS_RELEASE}
    - ssh -oStrictHostKeyChecking=no "$upload_host" flatpak build-update-repo "$UPLOAD_ROOT"

build-base-amd64:
  extends:
    - .build-base
    - .build-amd64

build-base-arm64:
  extends:
    - .build-base
    - .build-arm64

build-base-armhf:
  extends:
    - .build-base
    - .build-armhf

build-runtime-amd64:
  extends:
    - .build-runtime
    - .build-amd64
  needs:
    - prepare-build-env
    - build-base-amd64

build-runtime-arm64:
  extends:
    - .build-runtime
    - .build-arm64
  needs:
    - prepare-build-env
    - build-base-arm64

build-runtime-armhf:
  extends:
    - .build-runtime
    - .build-armhf
  needs:
    - prepare-build-env
    - build-base-armhf

build-app-amd64:
  extends:
    - .build-app
    - .build-amd64
  needs:
    - prepare-build-env
    - build-base-amd64
    - build-runtime-amd64

build-app-arm64:
  extends:
    - .build-app
    - .build-arm64
  needs:
    - prepare-build-env
    - build-base-arm64
    - build-runtime-arm64

build-app-armhf:
  extends:
    - .build-app
    - .build-armhf
  needs:
    - prepare-build-env
    - build-base-armhf
    - build-runtime-armhf

publish-runtime-amd64:
  extends:
    - .publish-runtime
    - .build-amd64
  needs:
    - prepare-build-env
    - build-runtime-amd64

publish-runtime-arm64:
  extends:
    - .publish-runtime
    - .build-arm64
  needs:
    - prepare-build-env
    - build-runtime-arm64

publish-runtime-armhf:
  extends:
    - .publish-runtime
    - .build-armhf
  needs:
    - prepare-build-env
    - build-runtime-armhf

publish-app-amd64:
  extends:
    - .publish-app
    - .build-amd64
  needs:
    - prepare-build-env
    - build-app-amd64

publish-app-arm64:
  extends:
    - .publish-app
    - .build-arm64
  needs:
    - prepare-build-env
    - build-app-arm64

publish-app-armhf:
  extends:
    - .publish-app
    - .build-armhf
  needs:
    - prepare-build-env
    - build-app-armhf
