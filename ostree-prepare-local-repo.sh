#!/bin/sh

set -eu

METADATA_ONLY="--commit-metadata-only"

opts=$(getopt -o "f" -l "full-commit" -- "$@")
eval set -- "$opts"

while [ $# -gt 0 ]; do
    case $1 in
        -f | --full-commit) METADATA_ONLY=""; shift ;;
        --) shift; break;;
        *) ;;
    esac
done

if [ $# -ne 3 ]
then
  echo "Usage: $0 [-f | --full-commit] REPO PULL_URL BRANCH" >&2
  exit 1
fi

repo=$1
ostree_pull_url=$2
branch=$3

mkdir -p "${repo}"
ostree init --repo="${repo}" --mode archive-z2
ostree remote --repo="${repo}" add --no-gpg-verify --if-not-exists origin "${ostree_pull_url}"

echo Pulling from "${ostree_pull_url}/refs/heads/${branch}"
http_code=$(curl --location --silent -o /dev/null --head -w "%{http_code}" "${ostree_pull_url}/refs/heads/${branch}")
case "$http_code" in
  200)
    ostree pull --repo="${repo}" --depth=-1 --mirror ${METADATA_ONLY} --disable-fsync origin "${branch}"
    ostree show --repo="${repo}" "${branch}"
    ;;
  404)
    echo "No remote ${branch} branch found"
    ;;
  *)
    echo "Error: Got HTTP '$http_code' trying to fetch '${ostree_pull_url}/refs/heads/${branch}'"
    exit 1
    ;;
esac
